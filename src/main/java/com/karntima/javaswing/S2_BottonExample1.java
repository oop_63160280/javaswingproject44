/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.karntima.javaswing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *
 * @author User
 */
public class S2_BottonExample1 {

    public static void main(String[] args) {
        JFrame f = new JFrame("Button Example");
        f.setSize(400, 300);
        f.setLayout(null);

        final JTextField tf = new JTextField();
        tf.setBounds(50, 50, 150, 20);
        f.add(tf);

        JButton b = new JButton("Click Here");
        b.setBounds(50, 100, 95, 30);
        f.add(b);

        b.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                tf.setText("Welcome to Javatpoint.");
            }
        });
        f.add(b);f.add(tf);  
        f.setVisible(true);
    }
}
